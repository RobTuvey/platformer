﻿using Assets.Scripts.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Managers
{
    public class TestManager : MonoBehaviour
    {
        #region REGION: Private Fields

        private readonly IInputManager _inputManager;

        #endregion

        public TestManager()
        {
            _inputManager = GameManager.Instance.InputManager;
        }

        private void Awake()
        {
            _inputManager.RegisterCallback("LEFT",  this, OnLeftButtonPressed);
            _inputManager.RegisterCallback("RIGHT", this, OnRightButtonPressed);
            _inputManager.RegisterCallback("A",     this, OnAButtonPressed);
            _inputManager.RegisterCallback("B",     this, OnBButtonPressed);
        }

        private void OnDestroy()
        {
            _inputManager.UnregisterCallback("LEFT",  this);
            _inputManager.UnregisterCallback("RIGHT", this);
            _inputManager.UnregisterCallback("A",     this);
            _inputManager.UnregisterCallback("B",     this);
        }

        private void Update()
        {
            if (true == _inputManager.IsButtonDown("LEFT"))
            {
                LoggerService.Info("Left button is down.");
            }

            if (true == _inputManager.IsButtonDown("RIGHT"))
            {
                LoggerService.Info("Right button is down.");
            }

            if (true == _inputManager.IsButtonDown("A"))
            {
                LoggerService.Info("A button is down.");
            }

            if (true == _inputManager.IsButtonDown("B"))
            {
                LoggerService.Info("B button is down.");
            }
        }

        public void OnLeftButtonPressed()
        {
            LoggerService.Info("Left arrow pressed.");
        }

        public void OnRightButtonPressed()
        {
            LoggerService.Info("Right arrow pressed.");
        }

        public void OnAButtonPressed()
        {
            LoggerService.Info("A button pressed.");
        }

        public void OnBButtonPressed()
        {
            LoggerService.Info("B button pressed.");
        }
    }
}
