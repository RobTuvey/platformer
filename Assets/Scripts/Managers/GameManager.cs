﻿using Assets.Scripts.Managers;
using Assets.Scripts.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    #region REGION: Constants

    private const string SCENES_PATH = "Assets/Scenes/";

    #endregion

    #region REGION: Properties

    public static GameManager Instance { get; private set; }

    public ILevelManager LevelManager { get; private set; }
    public IInputManager InputManager { get; private set; }

    public string StartupScene;

    #endregion

    private void Awake()
    {
        if (default(GameManager) != Instance && this != Instance)
        {
            Destroy(gameObject);
        }
        else
        {
            Initialisation();

            StartUp();
        }
    }

    private void Initialisation()
    {
        LoggerService.Verbose($"Initialising game manager.");

        Instance = this;

        LevelManager = gameObject.AddComponent<LevelManager>();
        InputManager = gameObject.AddComponent<InputManager>();

        LoggerService.Verbose($"Game manager initialised.");
    }

    private void StartUp()
    {
        if (false == string.IsNullOrWhiteSpace(StartupScene))
        {
            string scene = $"{SCENES_PATH}{StartupScene}.unity";
            LevelManager.ChangeScene(scene);
        }
    }
}
