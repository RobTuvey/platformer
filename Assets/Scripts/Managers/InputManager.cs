﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Managers
{
    public interface IInputManager
    {
        /// <summary>
        /// Register a callback using the given action, for the
        /// input and owner specified.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="owner">The owner object.</param>
        /// <param name="action">The callback action.</param>
        void RegisterCallback(string input, object owner, Action action);

        /// <summary>
        /// Unregister the given input for the owner specified.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="owner">The owner object.</param>
        void UnregisterCallback(string input, object owner);

        /// <summary>
        /// Trigger callbacks for the given input.
        /// </summary>
        /// <param name="input">The input.</param>
        void Input(string input);

        /// <summary>
        /// Trigger on button down callbacks for the given input.
        /// </summary>
        /// <param name="input">The input.</param>
        void OnButtonDown(string input);

        /// <summary>
        /// Trigger on button up callbacks for the given input.
        /// </summary>
        /// <param name="input">The input.</param>
        void OnButtonUp(string input);

        /// <summary>
        /// Checks to see if the given input is currently registered as down.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>Boolean inddicating if input is currently down.</returns>
        bool IsButtonDown(string input);
    }

    public class InputManager : MonoBehaviour, IInputManager
    {
        #region REGION: Constants

        public const string LEFT_BUTTON  = "LEFT";
        public const string RIGHT_BUTTON = "RIGHT";
        public const string A_BUTTON     = "A";
        public const string B_BUTTON     = "B";

        #endregion

        #region REGION: Private Fields

        private readonly List<Tuple<string, object, Action>> _callbacks;
        private readonly ICollection<string>                 _downInputs;

        #endregion

        public InputManager()
        {
            _callbacks  = new List<Tuple<string, object, Action>>();
            _downInputs = new List<string>();
        }

        /// <summary>
        /// Register a callback using the given action, for the
        /// input and owner specified.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="owner">The owner object.</param>
        /// <param name="action">The callback action.</param>
        public void RegisterCallback(string input, object owner, Action action)
        {
            if (false == string.IsNullOrWhiteSpace(input) && null != owner && default(Action) != action)
            {
                Tuple<string, object, Action> callback = new Tuple<string, object, Action>(input, owner, action);

                _callbacks.Add(callback);
            }
        }

        /// <summary>
        /// Unregister the given input for the owner specified.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <param name="owner">The owner object.</param>
        public void UnregisterCallback(string input, object owner)
        {
            if (false == string.IsNullOrWhiteSpace(input) && null != owner)
            {
                IEnumerable<Tuple<string, object, Action>> callbacks = _callbacks.Where(callback => callback.Item1 == input &&
                                                                                                    callback.Item2 == owner)
                                                                                 .ToList();

                foreach (Tuple<string, object, Action> callback in callbacks)
                {
                    _callbacks.Remove(callback);
                }
            }
        }

        /// <summary>
        /// Trigger callbacks for the given input.
        /// </summary>
        /// <param name="input">The input.</param>
        public void Input(string input)
        {
            if (false == string.IsNullOrWhiteSpace(input))
            {
                IEnumerable<Tuple<string, object, Action>> callbacks = _callbacks.Where(callback => callback.Item1 == input)
                                                                                 .ToList();

                foreach (Tuple<string, object, Action> callback in callbacks)
                {
                    callback.Item3.Invoke();
                }
            }
        }

        /// <summary>
        /// Trigger on button down callbacks for the given input.
        /// </summary>
        /// <param name="input">The input.</param>
        public void OnButtonDown(string input)
        {
            if (false == _downInputs.Contains(input))
            {
                _downInputs.Add(input);
            }
        }

        /// <summary>
        /// Trigger on button up callbacks for the given input.
        /// </summary>
        /// <param name="input">The input.</param>
        public void OnButtonUp(string input)
        {
            if (true == _downInputs.Contains(input))
            {
                _downInputs.Remove(input);
            }
        }

        /// <summary>
        /// Checks to see if the given input is currently registered as down.
        /// </summary>
        /// <param name="input">The input.</param>
        /// <returns>Boolean inddicating if input is currently down.</returns>
        public bool IsButtonDown(string input)
        {
            bool isDown = _downInputs.Contains(input);

            return isDown;
        }
    }
}
