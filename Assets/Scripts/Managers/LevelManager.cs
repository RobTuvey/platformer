﻿using Assets.Scripts.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace Assets.Scripts.Managers
{
    public interface ILevelManager
    {
        /// <summary>
        /// Change the currently loaded scene to the scene at the path provided.
        /// </summary>
        /// <param name="path">The scene path.</param>
        void ChangeScene(string path);
    }

    public class LevelManager : MonoBehaviour, ILevelManager
    {
        #region REGION: Properties

        #endregion

        #region REGION: Private Fields

        private Scene  _currentScene;
        private string _currentScenePath;

        #endregion

        /// <summary>
        /// Change the currently loaded scene to the scene at the path provided.
        /// </summary>
        /// <param name="path">The scene path.</param>
        public void ChangeScene(string path)
        {
            if (true == string.IsNullOrWhiteSpace(path))
            {
                LoggerService.Error("Invalid path; refusing to change scene.");
            }

            if (false == string.IsNullOrWhiteSpace(_currentScenePath))
            {
                StartCoroutine(UnloadScene());
            }

            StartCoroutine(LoadScene(path));
        }

        /// <summary>
        /// Load the scene at the given path.
        /// </summary>
        /// <param name="path">The scene path.</param>
        /// <returns></returns>
        private IEnumerator LoadScene(string path)
        {
            LoggerService.Verbose($"Loading scene at '{path}'.");

            AsyncOperation loadScene = SceneManager.LoadSceneAsync(path, LoadSceneMode.Additive);
            while (default(AsyncOperation) != loadScene && false == loadScene.isDone)
            {
                yield return null;
            }

            if (default(AsyncOperation) != loadScene)
            {
                _currentScene     = SceneManager.GetSceneByPath(path);
                _currentScenePath = path;

                LoggerService.Info($"Successfully loaded scene: '{_currentScene.name}'.");
            }
            else
            {
                LoggerService.Error($"Failed to load scene: '{path}'.");
            }
        }

        /// <summary>
        /// Unload the current scene.
        /// </summary>
        /// <returns></returns>
        private IEnumerator UnloadScene()
        {
            LoggerService.Info($"Unloading scene: '{_currentScene.name}'.");

            AsyncOperation unloadScene = SceneManager.UnloadSceneAsync(_currentScene);
            while (false == unloadScene.isDone)
            {
                yield return null;
            }

            LoggerService.Info($"Successfully unloaded scene: '{_currentScene.name}'.");

            _currentScene = default(Scene);
            _currentScenePath = string.Empty;
        }
    }
}
