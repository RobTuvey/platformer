﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Utilities
{
    public enum LoggingLevel
    {
        Verbose,
        Info,
        Warning,
        Error
    }

    public class LoggerService
    {
        public static LoggingLevel LogLevel = LoggingLevel.Verbose;

        public static void Verbose(string message)
        {
            Log(message, LoggingLevel.Verbose);
        }

        public static void Info(string message)
        {
            Log(message, LoggingLevel.Info);
        }

        public static void Warning(string message)
        {
            Log(message, LoggingLevel.Warning);
        }

        public static void Error(string message)
        {
            Log(message, LoggingLevel.Error);
        }

        private static void Log(string message, LoggingLevel level)
        {
            string log = $"Log {DateTime.Now} [{level.ToString().ToUpper()}]: {message}";

            if (LogLevel <= level)
            {
                Debug.Log(log);
            }
        }
    }
}
