﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Assets.Scripts.Controllers
{
    public class ButtonController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
    {
        #region REGION: Properties

        public string InputEvent;

        #endregion

        private void Awake()
        {
            Button button = gameObject.GetComponent<Button>();
        }

        public void OnPointerDown(PointerEventData eventData)
        {
            GameManager.Instance.InputManager.OnButtonDown(InputEvent);
            GameManager.Instance.InputManager.Input(InputEvent);
        }

        public void OnPointerUp(PointerEventData eventData)
        {
            GameManager.Instance.InputManager.OnButtonUp(InputEvent);
        }
    }
}
