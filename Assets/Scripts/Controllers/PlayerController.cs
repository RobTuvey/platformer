﻿using Assets.Scripts.Managers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Controllers
{
    public class PlayerController : MonoBehaviour
    {
        #region REGION: Constants

        private const string GROUND_TAG = "Ground";

        #endregion

        #region REGION: Properties

        public float MovementForce;
        public float HorizontalDrag;
        public float JumpForce;
        public float InAirGravity;
        public float NormalGravity;
        public float MaxCameraDistance;

        #endregion

        #region REGION: Private Fields

        private IInputManager _inputManager;
        private Rigidbody2D   _rigidBody;
        private bool          _isGrounded;
        private Camera        _camera;

        #endregion

        private void Awake()
        {
            _inputManager = GameManager.Instance.InputManager;
            _rigidBody    = gameObject.GetComponent<Rigidbody2D>();
            _camera       = Camera.main;
        }

        private void Start()
        {
            _inputManager.RegisterCallback(InputManager.A_BUTTON, this, OnJumpPressed);
        }

        private void FixedUpdate()
        {
            if (true == _inputManager.IsButtonDown(InputManager.LEFT_BUTTON))
            {
                _rigidBody.AddForce(Vector2.left * MovementForce);
            }

            if (true == _inputManager.IsButtonDown(InputManager.RIGHT_BUTTON))
            {
                _rigidBody.AddForce(Vector2.right * MovementForce);
            }

            _rigidBody.gravityScale = NormalGravity;
            if (true == _inputManager.IsButtonDown(InputManager.A_BUTTON) && false == _isGrounded)
            {
                _rigidBody.gravityScale = InAirGravity;
            }

            Vector2 velocity    = _rigidBody.velocity;
            velocity            = new Vector2(velocity.x * HorizontalDrag, velocity.y);
            _rigidBody.velocity = velocity;

            Vector2 position       = transform.position;
            Vector2 cameraPosition = _camera.transform.position;
            if (MaxCameraDistance < (position - cameraPosition).magnitude)
            {
                Vector2 cameraVelocity     = Vector2.zero;
                cameraPosition             = Vector2.SmoothDamp(cameraPosition, position, ref velocity, 0.1f);
                _camera.transform.position = new Vector3(cameraPosition.x, cameraPosition.y, _camera.transform.position.z);
            }
        }

        public void OnJumpPressed()
        {
            if (true == _isGrounded)
            {
                _rigidBody.AddForce(Vector2.up * JumpForce);
            }
        }

        private void OnTriggerEnter2D(Collider2D collision)
        {
            if (GROUND_TAG == collision.gameObject.tag)
            {
                _isGrounded     = true;
            }
        }

        private void OnTriggerExit2D(Collider2D collision)
        {
            if (GROUND_TAG == collision.gameObject.tag)
            {
                _isGrounded     = false;
            }
        }
    }
}
